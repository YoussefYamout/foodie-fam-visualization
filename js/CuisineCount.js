var cuisinedata = [
    {
        "group": "African",
        "value": 1
    },
    {
        "group": "American",
        "value": 576
    },
    {
        "group": "Argentine",
        "value": 2
    },
    {
        "group": "Armenian",
        "value": 32
    },
    {
        "group": "Asian",
        "value": 35
    },
    {
        "group": "Bakery",
        "value": 461
    },
    {
        "group": "Beverages",
        "value": 333
    },
    {
        "group": "Brazilian",
        "value": 1
    },
    {
        "group": "British",
        "value": 6
    },
    {
        "group": "Burger",
        "value": 472
    },
    {
        "group": "Canadian",
        "value": 3
    },
    {
        "group": "Chinese",
        "value": 42
    },
    {
        "group": "Coffee",
        "value": 404
    },
    {
        "group": "Continental",
        "value": 29
    },
    {
        "group": "Crepes",
        "value": 97
    },
    {
        "group": "Desserts",
        "value": 721
    },
    {
        "group": "Drinks Only",
        "value": 63
    },
    {
        "group": "Egyptian",
        "value": 1
    },
    {
        "group": "Ethiopian",
        "value": 3
    },
    {
        "group": "Falafel",
        "value": 154
    },
    {
        "group": "Fast Food",
        "value": 717
    },
    {
        "group": "Finger Food",
        "value": 52
    },
    {
        "group": "Foul",
        "value": 83
    },
    {
        "group": "French",
        "value": 146
    },
    {
        "group": "Fusion",
        "value": 41
    },
    {
        "group": "German",
        "value": 6
    },
    {
        "group": "Greek",
        "value": 4
    },
    {
        "group": "Gulf Food",
        "value": 7
    },
    {
        "group": "Hawaiian",
        "value": 7
    },
    {
        "group": "Healthy Food",
        "value": 94
    },
    {
        "group": "Ice Cream",
        "value": 203
    },
    {
        "group": "Indian",
        "value": 13
    },
    {
        "group": "Iranian",
        "value": 4
    },
    {
        "group": "Iraqi",
        "value": 5
    },
    {
        "group": "Irish",
        "value": 1
    },
    {
        "group": "Italian",
        "value": 398
    },
    {
        "group": "Japanese",
        "value": 108
    },
    {
        "group": "Juices",
        "value": 284
    },
    {
        "group": "Kaak",
        "value": 137
    },
    {
        "group": "Latin American",
        "value": 4
    },
    {
        "group": "Lebanese",
        "value": 2198
    },
    {
        "group": "Manakish",
        "value": 685
    },
    {
        "group": "Mediterranean",
        "value": 47
    },
    {
        "group": "Mexican",
        "value": 49
    },
    {
        "group": "Moroccan",
        "value": 2
    },
    {
        "group": "Patisserie",
        "value": 241
    },
    {
        "group": "Persian",
        "value": 2
    },
    {
        "group": "Peruvian",
        "value": 3
    },
    {
        "group": "Pizza",
        "value": 564
    },
    {
        "group": "Pub Food",
        "value": 55
    },
    {
        "group": "Russian",
        "value": 1
    },
    {
        "group": "Saj",
        "value": 189
    },
    {
        "group": "Salad",
        "value": 465
    },
    {
        "group": "Sandwich",
        "value": 742
    },
    {
        "group": "Seafood",
        "value": 247
    },
    {
        "group": "Shawarma",
        "value": 179
    },
    {
        "group": "Spanish",
        "value": 5
    },
    {
        "group": "Steak",
        "value": 35
    },
    {
        "group": "Sushi",
        "value": 132
    },
    {
        "group": "Syrian",
        "value": 5
    },
    {
        "group": "Tapas",
        "value": 5
    },
    {
        "group": "Tea",
        "value": 8
    },
    {
        "group": "Tex-Mex",
        "value": 5
    },
    {
        "group": "Thai",
        "value": 8
    },
    {
        "group": "Turkish",
        "value": 8
    },
    {
        "group": "Vietnamese",
        "value": 1
    }
]

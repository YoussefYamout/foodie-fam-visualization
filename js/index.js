function initMap() {
    const myLatLng = {lat: 33.8547, lng: 35.8623};
    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 8,
        center: myLatLng,
    });
}
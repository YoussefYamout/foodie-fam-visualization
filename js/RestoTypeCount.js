var restotypedata = [
    {
        "group": "Bakery",
        "value": 381
    },
    {
        "group": "Bar",
        "value": 100
    },
    {
        "group": "Beverage Shop",
        "value": 265
    },
    {
        "group": "Café",
        "value": 256
    },
    {
        "group": "Casual Dining",
        "value": 1378
    },
    {
        "group": "Club",
        "value": 22
    },
    {
        "group": "Dessert Parlor",
        "value": 508
    },
    {
        "group": "Fine Dining",
        "value": 88
    },
    {
        "group": "Food Court",
        "value": 12
    },
    {
        "group": "Food Truck",
        "value": 22
    },
    {
        "group": "Furn",
        "value": 635
    },
    {
        "group": "Kiosk",
        "value": 19
    },
    {
        "group": "Lounge",
        "value": 45
    },
    {
        "group": "Patisserie",
        "value": 230
    },
    {
        "group": "Pub",
        "value": 168
    },
    {
        "group": "Quick Bites",
        "value": 1805
    },
    {
        "group": "Wine Bar",
        "value": 8
    }
]

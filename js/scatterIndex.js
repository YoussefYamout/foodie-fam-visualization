import {
    select,
    csv,
    scaleLinear,
    extent,
    axisLeft,
    axisBottom
} from 'd3';
import {dropdownMenu} from '../../../../Desktop/685/dropdownMenu';
import {scatterPlot} from './scatterPlot';

const svg = d3.select('svg');

const width = +svg.attr('width');
const height = +svg.attr('height');

let data;
let xColumn;
//let yColumn;


const onXColumnClicked = column => {
    xColumn = column;
    render();
};

const render = () => {

    select('#menus')
        .call(dropdownMenu, {  //add menu to the body element
            options: data.columns, //options saro kell lcolumns lmwjudin
            onOptionClicked: onXColumnClicked
        });

    svg.call(scatterPlot, {
        title: 'Review VS. Rating',
        xValue: d => d[xColumn],
        //xAxisLabel: xColumn,
        yValue: d => d.rating,
        circleRadius: 2.5,
        yAxisLabel: 'Rating',
        margin: {top: 60, right: 60, bottom: 130, left: 190},
        width,
        height,
        data
    });

};

d3.csv('../data/data.csv').then(loadedData => {
    data = loadedData;
    data.forEach(d => {
        d.raiting = +d.rating;
        d.reviews = +d.reviews;
        d.costfor2 = +d.costfor2;
        // d.RestoAddress = d.RestoAddress;
    });
    xColumn = data.columns[0];
    render();
});




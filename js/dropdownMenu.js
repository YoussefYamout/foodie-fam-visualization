export const dropdownMenu = (selection, props) => { //create drpdwn function that accept as input a selection and props
    const {
        options,
        onOptionClicked,
        selectedOption,
    } = props;

    let select = selection.selectAll('select').data([null]);
    select = select.enter().append('select') //reassigne select to be a merge of an  update select
        .merge(select)
        .on('change', function () {
            onOptionClicked(this.value);//passing the value to onOptionClicked
        });

    const option = select.selectAll('option').data(options); //we pass for data the options array//select contain only 1 element
    option.enter().append('option') //we need to use the update pattern
        .merge(option)
        .attr('value', d => d)
        .text(d => d);
};
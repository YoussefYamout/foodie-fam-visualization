var AvgCostPerArea =
    [
        {
            "group": "Aabra",
            "value": 19722
        },
        {
            "group": "Aannaya",
            "value": 36765
        },
        {
            "group": "Aaramoun",
            "value": 17195
        },
        {
            "group": "Adma",
            "value": 60909
        },
        {
            "group": "Aicha Bakkar",
            "value": 14439
        },
        {
            "group": "Ain el Remmaneh",
            "value": 17100
        },
        {
            "group": "Ain Mreisse",
            "value": 56935
        },
        {
            "group": "Ain Saade",
            "value": 42500
        },
        {
            "group": "Aintoura",
            "value": 26000
        },
        {
            "group": "Ajaltoun",
            "value": 36200
        },
        {
            "group": "Al Mina",
            "value": 24023
        },
        {
            "group": "Aley",
            "value": 33110
        },
        {
            "group": "Amchit",
            "value": 50111
        },
        {
            "group": "Amioun",
            "value": 32500
        },
        {
            "group": "Anfeh",
            "value": 57174
        },
        {
            "group": "Anjar",
            "value": 57000
        },
        {
            "group": "Antelias",
            "value": 56122
        },
        {
            "group": "Araya",
            "value": 14000
        },
        {
            "group": "Ashrafieh",
            "value": 44468
        },
        {
            "group": "Baabda",
            "value": 26607
        },
        {
            "group": "Baabdat",
            "value": 40333
        },
        {
            "group": "Baakline",
            "value": 21000
        },
        {
            "group": "Badaro",
            "value": 42625
        },
        {
            "group": "Balamand",
            "value": 51500
        },
        {
            "group": "Ballouneh",
            "value": 24733
        },
        {
            "group": "Baouchriye",
            "value": 19667
        },
        {
            "group": "Barbour",
            "value": 15333
        },
        {
            "group": "Barouk",
            "value": 40000
        },
        {
            "group": "Baskinta",
            "value": 46875
        },
        {
            "group": "Basta",
            "value": 11333
        },
        {
            "group": "Batroun",
            "value": 47379
        },
        {
            "group": "Bchamoun",
            "value": 14773
        },
        {
            "group": "Beit Mery",
            "value": 43600
        },
        {
            "group": "Beiteddine",
            "value": 43500
        },
        {
            "group": "Bhamdoun",
            "value": 44250
        },
        {
            "group": "Bikfaya",
            "value": 39434
        },
        {
            "group": "Bkassine",
            "value": 35000
        },
        {
            "group": "Bnachii",
            "value": 41364
        },
        {
            "group": "Bourj Abi Haidar",
            "value": 10789
        },
        {
            "group": "Bourj Hammoud",
            "value": 15447
        },
        {
            "group": "Breij",
            "value": 60000
        },
        {
            "group": "Broumanna",
            "value": 51518
        },
        {
            "group": "Bsalim",
            "value": 15278
        },
        {
            "group": "Bteghrine",
            "value": 13571
        },
        {
            "group": "Cedars",
            "value": 47500
        },
        {
            "group": "Chekka",
            "value": 32647
        },
        {
            "group": "Chiyah",
            "value": 17204
        },
        {
            "group": "Choueifat",
            "value": 17679
        },
        {
            "group": "Chtaura",
            "value": 33000
        },
        {
            "group": "Clemenceau",
            "value": 37000
        },
        {
            "group": "Cola",
            "value": 9333
        },
        {
            "group": "Concorde Centre",
            "value": 20000
        },
        {
            "group": "Cornet Chahwan",
            "value": 29706
        },
        {
            "group": "Damour",
            "value": 34118
        },
        {
            "group": "Dbayeh",
            "value": 69408
        },
        {
            "group": "Deir El Qamar",
            "value": 41053
        },
        {
            "group": "Dekwaneh",
            "value": 21442
        },
        {
            "group": "Dhour Choueir",
            "value": 29118
        },
        {
            "group": "Dora",
            "value": 23077
        },
        {
            "group": "Douma",
            "value": 49375
        },
        {
            "group": "Downtown",
            "value": 42500
        },
        {
            "group": "Ehden",
            "value": 39690
        },
        {
            "group": "Ehmej",
            "value": 17000
        },
        {
            "group": "Falougha",
            "value": 35000
        },
        {
            "group": "Fanar",
            "value": 21000
        },
        {
            "group": "Faqra Kfardebian",
            "value": 91818
        },
        {
            "group": "Faraya",
            "value": 32956
        },
        {
            "group": "Feytroun",
            "value": 38889
        },
        {
            "group": "Fraidis",
            "value": 24375
        },
        {
            "group": "Furn El Chebbak",
            "value": 21607
        },
        {
            "group": "Gemmayze",
            "value": 58365
        },
        {
            "group": "Ghazir",
            "value": 50729
        },
        {
            "group": "Hadath",
            "value": 24697
        },
        {
            "group": "Halat",
            "value": 56765
        },
        {
            "group": "Hammana",
            "value": 30136
        },
        {
            "group": "Hamra",
            "value": 34465
        },
        {
            "group": "Hamra Square Center",
            "value": 25000
        },
        {
            "group": "Hamra Urban Gardens",
            "value": 47500
        },
        {
            "group": "Haret Hreik",
            "value": 22317
        },
        {
            "group": "Haret Saida",
            "value": 16647
        },
        {
            "group": "Harissa",
            "value": 37000
        },
        {
            "group": "Hasroun",
            "value": 33125
        },
        {
            "group": "Hazmieh",
            "value": 28472
        },
        {
            "group": "Hlaliyeh",
            "value": 22188
        },
        {
            "group": "Hrajel",
            "value": 37692
        },
        {
            "group": "Jal el Dib",
            "value": 28558
        },
        {
            "group": "Jbeil",
            "value": 51069
        },
        {
            "group": "Jdeide",
            "value": 25133
        },
        {
            "group": "Jeita",
            "value": 45556
        },
        {
            "group": "Jezzine",
            "value": 35294
        },
        {
            "group": "Jiyeh",
            "value": 18500
        },
        {
            "group": "Jnah",
            "value": 30769
        },
        {
            "group": "Joint Beirut",
            "value": 15000
        },
        {
            "group": "Jounieh",
            "value": 42434
        },
        {
            "group": "Karantina",
            "value": 28143
        },
        {
            "group": "Kaslik",
            "value": 51500
        },
        {
            "group": "Kfardebian",
            "value": 68667
        },
        {
            "group": "Kfarhbab Gate Plaza",
            "value": 31667
        },
        {
            "group": "Kfarshima",
            "value": 23000
        },
        {
            "group": "Khalde",
            "value": 28750
        },
        {
            "group": "Koleiat",
            "value": 50000
        },
        {
            "group": "Koreitem",
            "value": 23125
        },
        {
            "group": "Kousba",
            "value": 34231
        },
        {
            "group": "Laqlouq",
            "value": 61429
        },
        {
            "group": "LeMall Dbayeh",
            "value": 50000
        },
        {
            "group": "Manara",
            "value": 42115
        },
        {
            "group": "Mansourieh",
            "value": 22162
        },
        {
            "group": "Mar Elias",
            "value": 14565
        },
        {
            "group": "Mar Mikhael",
            "value": 57406
        },
        {
            "group": "Mayrouba",
            "value": 52462
        },
        {
            "group": "Mazraa",
            "value": 16486
        },
        {
            "group": "Mazraat Yachouh",
            "value": 19792
        },
        {
            "group": "Minet El Hosn",
            "value": 112045
        },
        {
            "group": "Mkalles",
            "value": 21000
        },
        {
            "group": "Mrouj",
            "value": 28077
        },
        {
            "group": "Msaytbeh",
            "value": 14943
        },
        {
            "group": "Mtayleb",
            "value": 36579
        },
        {
            "group": "Nabatiyeh",
            "value": 28448
        },
        {
            "group": "Naccache",
            "value": 27500
        },
        {
            "group": "Nowayri",
            "value": 11000
        },
        {
            "group": "Okaibe",
            "value": 47813
        },
        {
            "group": "Raifoun",
            "value": 42500
        },
        {
            "group": "Ramlet El Bayda",
            "value": 63182
        },
        {
            "group": "Raouche",
            "value": 54500
        },
        {
            "group": "Ras El Nabeh",
            "value": 21818
        },
        {
            "group": "Ras Osta",
            "value": 29167
        },
        {
            "group": "Rmayleh",
            "value": 29318
        },
        {
            "group": "Roumieh",
            "value": 38750
        },
        {
            "group": "Royal Tulip Achrafieh Hotel",
            "value": 40000
        },
        {
            "group": "Sad El Baouchriye",
            "value": 14217
        },
        {
            "group": "Saida",
            "value": 21883
        },
        {
            "group": "Saifi",
            "value": 109286
        },
        {
            "group": "Salhieh",
            "value": 55500
        },
        {
            "group": "Sanayeh",
            "value": 12917
        },
        {
            "group": "Sin El Fil",
            "value": 23103
        },
        {
            "group": "Souq El Gharb",
            "value": 25000
        },
        {
            "group": "Tabarja",
            "value": 49822
        },
        {
            "group": "Tannourine",
            "value": 15000
        },
        {
            "group": "Tariq el Jdide",
            "value": 12692
        },
        {
            "group": "Tripoli",
            "value": 18550
        },
        {
            "group": "Tyre",
            "value": 32624
        },
        {
            "group": "Verdun",
            "value": 35513
        },
        {
            "group": "Verdun 732",
            "value": 50000
        },
        {
            "group": "Verdun Plaza",
            "value": 40000
        },
        {
            "group": "Wadi Chahrour",
            "value": 22500
        },
        {
            "group": "Zaarour",
            "value": 55625
        },
        {
            "group": "Zaarour Club",
            "value": 97500
        },
        {
            "group": "Zahle",
            "value": 27197
        },
        {
            "group": "Zalka",
            "value": 24583
        },
        {
            "group": "Zekrit",
            "value": 66000
        },
        {
            "group": "Zgharta",
            "value": 22565
        },
        {
            "group": "Zokak Al Blat",
            "value": 13333
        },
        {
            "group": "Zouk Mikayel",
            "value": 25588
        },
        {
            "group": "Zouk Mosbeh",
            "value": 25000
        },
        {
            "group": "Rmeileh",
            "value": 27500
        },
        {
            "group": "Rabieh",
            "value": 85000
        }
    ]
